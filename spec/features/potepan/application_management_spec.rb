require 'rails_helper'

RSpec.feature "共通部分のテスト(ヘッダー、フッター)", type: :feature do
  given(:taxon) { create(:taxon) }
  given(:product) { create(:product, taxons: [taxon]) }

  feature "ホーム画面へアクセス" do
    scenario "タイトルが正しく表示されること" do
      visit potepan_path
      expect(title).to eq "BIGBAG Store"
    end
  end

  feature "商品詳細ページへアクセス" do
    background do
      visit potepan_product_path(product.id)
    end

    scenario "ヘッダーの「home」を押下すると、ホーム画面へアクセスすること" do
      within(".header") do
        click_link 'Home'
      end
      expect(current_path).to eq potepan_path
    end

    scenario "「BIGBAN」を押下すると、ホーム画面へアクセスすること" do
      find('#logo').click
      expect(current_path).to eq potepan_path
    end

    scenario "商品詳細ページに合わせたタイトルが表示されること" do
      expect(title).to eq "#{product.name} - BIGBAG Store"
    end
  end
end
