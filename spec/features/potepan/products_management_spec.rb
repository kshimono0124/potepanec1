require 'rails_helper'

RSpec.feature "商品詳細ページの表示確認", type: :feature do
  given(:image_0) { create(:image) }
  given(:image_1) { create(:image) }
  given(:variant) { create(:variant, is_master: true, images: [image_0, image_1]) }
  given(:taxon) { create(:taxon) }
  given(:product) { create(:product, master: variant, taxons: [taxon]) }
  given(:product_id) { product.id }
  given(:related_image) { create(:image) }
  given(:related_variant) { create(:variant, is_master: true, images: [related_image]) }
  given!(:related_product) { create(:product, master: related_variant, taxons: [product.taxons.first], name: "#{product.name}_related") }

  background do
    visit potepan_product_path(product_id)
  end

  scenario "パンくずリストの「Home」をクリックしたときにホーム画面へアクセスすること" do
    within(".breadcrumb") do
      click_link 'Home'
    end
    expect(current_path).to eq potepan_path
  end

  feature "一覧ページへ戻るをクリックしたときに正しいページにアクセスすること" do
    scenario "一覧ページへ戻るをクリックしたときに正しいcategoriesのshowページにアクセスすること" do
      click_link '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  feature "商品情報が表示されること" do
    scenario "商品名が正しいこと" do
      expect(page).to have_selector '#product_name', text: product.name
    end

    scenario "値段が正しいこと" do
      expect(page).to have_selector '#product_display_price', text: product.display_price
    end

    scenario "商品説明が正しいこと" do
      expect(page).to have_selector '#product_description', text: product.description
    end

    feature "商品画像が正しいこと" do
      feature "大きい画像の表示" do
        scenario "image_large_0が正しく表示できていること" do
          expect(page).to have_selector "#image_large_0 > img[src*='/spree/products/#{product.images.first.id}/large/#{product.images.first.attachment_file_name}']"
        end

        scenario "image_large_1が正しく表示できていること" do
          expect(page).to have_selector "#image_large_1 > img[src*='/spree/products/#{product.images.second.id}/large/#{product.images.second.attachment_file_name}']"
        end
      end

      feature "小さい画像の表示" do
        scenario "image_small_0が正しく表示できていること" do
          expect(page).to have_selector "#image_small_0 > img[src*='/spree/products/#{product.images.first.id}/small/#{product.images.first.attachment_file_name}']"
        end

        scenario "image_small_1が正しく表示できていること" do
          expect(page).to have_selector "#image_small_1 > img[src*='/spree/products/#{product.images.second.id}/small/#{product.images.second.attachment_file_name}']"
        end
      end
    end
  end

  feature "関連商品の商品名、値段、画像が正しく表示されること" do
    scenario "クリックした時に、対象の商品詳細ページにアクセスすること。" do
      within(".productsContent") do
        expect(page).to have_selector 'h5', text: related_product.name
        expect(page).to have_selector 'h3', text: related_product.display_price
        expect(page).to have_selector ".productImage > img[src*='/spree/products/#{related_product.images.first.id}/large/#{related_product.images.first.attachment_file_name}']"

        click_link related_product.name
        expect(current_path).to eq potepan_product_path(related_product.id)
      end
    end
  end
end
