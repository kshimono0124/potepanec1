require 'rails_helper'

RSpec.feature "カテゴリーページの表示確認", type: :feature do
  given(:image) { create(:image) }
  given(:variant) { create(:variant, is_master: true, images: [image]) }
  given(:taxonomy) { create(:taxonomy) }
  given(:taxon) { create(:taxon, taxonomy_id: taxonomy.id) }
  given!(:product) { create(:product, master: variant, taxons: [taxon]) }
  given(:product_only) { create(:product, name: "#{product.name}_only") }

  background do
    visit potepan_category_path(taxon.id)
  end

  feature "サイドバーの商品カテゴリー" do
    scenario "正しいカテゴリーの大分類(taxonomy)と小分類(taxon)を表示されていること" do
      within(:css, '.navbar-side-collapse') do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content "#{taxon.name} (#{taxon.all_products.count})"
      end
    end
    scenario "カテゴリー(taxon)をクリックした時正しいカテゴリーページへ遷移すること" do
      within(:css, ".navbar-side-collapse") do
        click_link taxon.name, match: :first
      end
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  feature "商品情報が正しく表示できているか" do
    scenario "商品画像が正しく表示できていること" do
      expect(page).to have_selector "#product_image_0 > img[src*='/spree/products/#{product.images.first.id}/large/#{product.images.first.attachment_file_name}']"
    end

    scenario "商品名が正しく表示できていること" do
      expect(page).to have_selector "#product_name_0", text: product.name
    end

    scenario "値段が正しく表示できていること" do
      expect(page).to have_selector "#product_price_0", text: product.display_price
    end
  end

  scenario "サイドバーの商品数と表示している商品数が同じこと" do
    expect(all(".productBox").size).to eq taxon.all_products.count
  end

  scenario "taxon情報を持たないproductが表示されないこと" do
    within("div.productBox") do
      expect(page).not_to have_content product_only.name
    end
  end

  scenario "商品クリック時に正しい商品詳細ページに遷移できるか" do
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
