require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /index" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon)    { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let(:product) { create(:base_product, taxons: [taxon]) }
    let(:image) { build(:image) }

    before do
      product.images << image if product
      get potepan_category_path(taxon.id)
    end

    it "レスポンスコードが200であること" do
      expect(response).to have_http_status "200"
    end

    it 'カテゴリー(taxon)を取得する' do
      expect(controller.instance_variable_get('@taxon')).to eq taxon
    end

    it '商品情報(product)を取得する' do
      expect(controller.instance_variable_get('@products')).to contain_exactly(product)
    end

    it 'カテゴリーの大分類(taxonomy)が表示されること' do
      expect(response.body).to include taxonomy.name
    end

    it 'カテゴリー(taxon)が表示されること' do
      expect(response.body).to include taxon.name
    end
  end
end
