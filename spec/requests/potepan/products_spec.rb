require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }

  before do
    get potepan_product_path product.id
  end

  describe "商品詳細" do
    it "httpステータスが200を返すこと" do
      expect(response).to have_http_status "200"
    end

    it "意図したページへrenderできているか" do
      expect(response).to render_template(:show)
    end

    it "商品名と価格、商品説明が正しく表示されていること" do
      aggregate_failures do
        expect(response.body).to include product.name
        expect(response.body).to include product.price.to_s
        expect(response.body).to include product.description
      end
    end
  end

  describe "関連商品" do
    let!(:related_product) { create(:product, taxons: [taxon]) }

    it "関連商品が４つ表示される" do
      within ".productsContent" do
        expect(response.body).to have_selector(".productBox", count: 4)
      end
    end

    it "関連商品の情報が正しく表示されていること" do
      within ".productsContent" do
        expect(response.body).to have_content related_product.name
        expect(response.body).to have_content related_product.display_price
        expect(response.body).to have_content related_product.display_image.attachment(:large)
      end
    end
  end
end
