require 'rails_helper'

RSpec.describe 'Potepan::ProductDecorator', type: :model do
  let(:taxon_a)        { create(:taxon) }
  let(:taxon_b)        { create(:taxon) }
  let(:other_taxon)    { create(:taxon) }
  let(:product)        { create(:product, taxons: [taxon_a, taxon_b]) }
  let(:other_products) { create_list(:product, 2, name: "Others", taxons: [other_taxon]) }
  let(:products) do
    create_list(:product, 2, name: "Related", taxons: [taxon_a, taxon_b])
    create_list(:product, 3, name: "Related", taxons: [taxon_b])
  end

  describe "related_productsのテスト" do
    it "関連商品が存在し、正常であること" do
      expect(product.related_products).to include(*products)
    end

    it "関連しない商品が含まれていないこと" do
      expect(product.related_products).not_to include(*other_products)
    end

    it "関連商品には、選択中の商品自身が含まれないこと" do
      expect(product.related_products).not_to include product
    end

    it "重複が無いこと" do
      related_products = product.related_products
      expect(related_products).to eq related_products.uniq
    end
  end
end
